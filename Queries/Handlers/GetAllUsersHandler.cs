﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.User.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.User.Queries.Handlers
{
    public class GetAllUsersHandler : IQueryHandler<GetAllUsers, IEnumerable<DataModels.User>>
    {
        private readonly IRepository<DataModels.User, Guid> _userRepository;
        private readonly IRepository<Role, Guid> _roleRepository;

        public GetAllUsersHandler(IRepository<DataModels.User, Guid> userRepository, IRepository<Role, Guid> roleRepository)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }

        public async Task<IEnumerable<DataModels.User>> Handle(GetAllUsers request, CancellationToken cancellationToken)
        {
            var findQuery = request.MapTo<FindQuery<DataModels.User>>();
            if (request.StoreId.HasValue)
            {
                findQuery.Filters.Add(x => x.StoreId.Equals(request.StoreId));
            }
            var users = await _userRepository.FindAsync(findQuery, cancellationToken);

            foreach (var user in users)
            {
                user.Role = await _roleRepository.FindOneAsync(user.RoleId, cancellationToken);
            }

            return users;
        }
    }
}