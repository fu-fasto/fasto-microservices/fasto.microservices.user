﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.User.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;
using BC = BCrypt.Net.BCrypt;

namespace FastO.Microservices.User.Commands.Handler
{
    public class CreateUserHandler : ICommandHandler<CreateUser>
    {
        private readonly ILogger<CreateUserHandler> _logger;
        private readonly IRepository<DataModels.User, Guid> _userRepository;
        private readonly IRepository<Role, Guid> _roleRepository;

        public CreateUserHandler(ILogger<CreateUserHandler> logger, IRepository<DataModels.User, Guid> userRepository,
            IRepository<Role, Guid> roleRepository)
        {
            _logger = logger;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }
        
        public async Task Handle(CreateUser command, CancellationToken cancellationToken)
        {
            var user = command.MapTo<DataModels.User>();
            var role = await _roleRepository.FindOneAsync(command.RoleId, cancellationToken)
                       ?? throw new NullReferenceException($"Role id {command.RoleId} does not exist");
            user.Role = role;
            user.Password = BC.HashPassword(command.Password);
            await _userRepository.AddAsync(user, cancellationToken);
            _logger.LogInformation($"Created user with id={command.Id}");
        }
    }
}