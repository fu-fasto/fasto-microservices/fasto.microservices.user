﻿using System;
using MicroBoost.Types;

namespace FastO.Microservices.User.DataModels
{
    public class User : EntityBase<Guid>
    {
        /// <summary>
        /// User Id
        /// </summary>
        public override Guid Id { get; set; } = Guid.NewGuid();
        
        /// <summary>
        /// Role object
        /// </summary>
        public Role Role { get; set; }

        /// <summary>
        /// Role Id
        /// </summary>
        public Guid RoleId { get; set; }
        
        /// <summary>
        /// Store Id if exists
        /// </summary>
        public Guid? StoreId { get; set; }
        
        /// <summary>
        /// User Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// User Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }
    }
}