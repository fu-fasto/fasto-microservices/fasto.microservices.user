﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Persistence;

namespace FastO.Microservices.User.Commands.Handler
{
    public class UpdateUserHandler : ICommandHandler<UpdateUser>
    {
        private readonly IRepository<DataModels.User, Guid> _userRepository;

        public UpdateUserHandler(IRepository<DataModels.User, Guid> userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task Handle(UpdateUser command, CancellationToken cancellationToken)
        {
            var user = await _userRepository.FindOneAsync(command.Id, cancellationToken);

            user.Name = command.Name;
            user.Email = command.Email;
            user.RoleId = command.RoleId;
            user.StoreId = command.StoreId;

            await _userRepository.UpdateAsync(user, cancellationToken);
        }
    }
}