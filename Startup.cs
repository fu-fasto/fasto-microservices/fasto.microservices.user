using FastO.Microservices.User.DataModels;
using FastO.Microservices.User.Helpers;
using FastO.Microservices.User.Services;
using MicroBoost;
using MicroBoost.Caching;
using MicroBoost.Cqrs;
using MicroBoost.Jaeger;
using MicroBoost.Metrics;
using MicroBoost.Persistence;
using MicroBoost.Swagger;
using MicroBoost.WebAPI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FastO.Microservices.User
{
    public class Startup
    {
        private IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var builder = services
                .AddMicroBoostBuilder()
                .AddJaeger()
                .AddWebApi()
                .AddCqrs()
                .AddSqlPersistence<UserDbContext>()
                .AddCaching()
                .AddSwaggerDocs()
                .RegisterOptions<AppSetting>("AppSetting")
                .AddScoped<ILoginService, LoginService>()
                .Build();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app
                .UseMetrics()
                .UseJaeger()
                .UseWebApi()
                .UseSwaggerDocs();
        }
    }
}