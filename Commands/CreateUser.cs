﻿using System;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.User.Commands
{
    public class CreateUser : CommandBase
    {
        [MessageKey] public Guid Id { get; set; } = Guid.NewGuid();

        public Guid RoleId { get; set; }
        
        public Guid? StoreId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
    }
}