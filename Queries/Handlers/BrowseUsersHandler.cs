﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.User.Queries.Handlers
{
    public class BrowseUsersHandler : IQueryHandler<BrowseUsers, PagedResult<DataModels.User>>
    {
        private readonly IRepository<DataModels.User, Guid> _userRepository;

        public BrowseUsersHandler(IRepository<DataModels.User, Guid> userRepository)
        {
            _userRepository = userRepository;
        }
        
        public Task<PagedResult<DataModels.User>> Handle(BrowseUsers query, CancellationToken cancellationToken)
        {
            var findQuery = query.MapTo<FindQuery<DataModels.User>>();
            
            var rawDict =
                new Dictionary<string, Expression<Func<DataModels.User, object>>>(StringComparer.OrdinalIgnoreCase)
                {
                    {"Name", x => x.Name},
                };

            query.RawSorts?.ForEach(rawSort =>
            {
                if (rawDict.TryGetValue(rawSort.Field, out var selector))
                    findQuery.SortExpressions.Add(new SortExpression<DataModels.User>
                    {
                        Selector = selector,
                        IsAscending = rawSort.IsAscending
                    });
            });

            return _userRepository.BrowseAsync(findQuery, cancellationToken);
        }
    }
}