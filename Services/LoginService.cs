﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using FastO.Microservices.User.DataModels;
using FastO.Microservices.User.Helpers;
using MicroBoost.Auth.Identity.Dtos;
using MicroBoost.Helpers;
using MicroBoost.Persistence;
using Microsoft.IdentityModel.Tokens;
using BC = BCrypt.Net.BCrypt;

namespace FastO.Microservices.User.Services
{
    public interface ILoginService
    {
        LoginResponse Authenticate(LoginRequest request);
    }
    
    public class LoginService : ILoginService
    {
        private readonly IRepository<DataModels.User, Guid> _userRepository;
        private readonly IRepository<Role, Guid> _roleRepository;
        private readonly AppSetting _appSetting;

        public LoginService(AppSetting appSetting, IRepository<DataModels.User, Guid> userRepository,
            IRepository<Role, Guid> roleRepository)
        {
            _appSetting = appSetting;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }
        
        public LoginResponse Authenticate(LoginRequest request)
        {
            var user = _userRepository.FindOneAsync(x => x.Email == request.Email).Result;

            if (user == null || !BC.Verify(request.Password, user.Password)) return null;

            var role = _roleRepository.FindOneAsync(user.RoleId).Result.RoleName;

            var token = GenerateJwtToken(user, role);

            LoginResponse response = user.MapTo<LoginResponse>();

            response.AccessToken = token;

            response.Role = role.ToUpper();

            return response;
        }
        
        private string GenerateJwtToken(DataModels.User user, string role)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSetting.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                    {
                        new Claim("uid", user.Id.ToString()),
                        new Claim(ClaimTypes.Email, user.Email),
                        new Claim(ClaimTypes.Role, role),
                        new Claim("storeId", user.StoreId.ToString())
                    }
                ),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}