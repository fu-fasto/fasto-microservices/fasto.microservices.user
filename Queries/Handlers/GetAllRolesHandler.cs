﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.User.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.User.Queries.Handlers
{
    public class GetAllRolesHandler : IQueryHandler<GetAllRoles, IEnumerable<Role>>
    {
        private readonly IRepository<Role, Guid> _roleRepository;

        public GetAllRolesHandler(IRepository<Role, Guid> roleRepository)
        {
            _roleRepository = roleRepository;
        }
        
        public Task<IEnumerable<Role>> Handle(GetAllRoles request, CancellationToken cancellationToken)
        {
            var findQuery = request.MapTo<FindQuery<Role>>();

            if (request.StoreId.HasValue)
                findQuery.Filters.Add(x => x.RoleName.ToUpper().Contains("STORE"));
    
            return _roleRepository.FindAsync(findQuery, cancellationToken);
        }
    }
}