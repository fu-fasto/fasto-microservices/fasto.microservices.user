﻿using System;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.User.Commands
{
    public class ChangePassword : CommandBase
    {
        public Guid Id { get; set; }
        
        public string OldPwd { get; set; }

        public string NewPwd { get; set; }

        public string RetypeNewPwd { get; set; }
    }
}