﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.User.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.User.Commands.Handler
{
    public class CreateRoleHandler : ICommandHandler<CreateRole>
    {
        private readonly ILogger<CreateRoleHandler> _logger;
        private readonly IRepository<Role, Guid> _roleRepository;

        public CreateRoleHandler(ILogger<CreateRoleHandler> logger, IRepository<Role, Guid> roleRepository)
        {
            _logger = logger;
            _roleRepository = roleRepository;
        }
        
        public async Task Handle(CreateRole command, CancellationToken cancellationToken)
        {
            var role = command.MapTo<Role>();
            await _roleRepository.AddAsync(role, cancellationToken);
            _logger.LogInformation($"Created role with id={command.Id} and name={command.RoleName}");
        }
    }
}