﻿using System;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.User.Commands
{
    public class UpdateUser : CommandBase
    {
        [MessageKey] public Guid Id { get; set; }

        public Guid RoleId { get; set; }
        
        public Guid? StoreId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
    }
}