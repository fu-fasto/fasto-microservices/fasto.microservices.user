﻿using System;
using FastO.Microservices.User.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.User.Queries
{
    public class GetAllRoles : AllQueryBase<Role>
    {
        public Guid? StoreId { get; set; }
    }
}