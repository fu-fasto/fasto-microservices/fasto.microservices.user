﻿using Microsoft.EntityFrameworkCore;

namespace FastO.Microservices.User.DataModels
{
    public sealed class UserDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

        public UserDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().ToTable("users");
            modelBuilder.Entity<User>().Property(x => x.Id).HasColumnName("id");
            modelBuilder.Entity<User>().Property(x => x.RoleId).HasColumnName("role_id");
            modelBuilder.Entity<User>().Property(x => x.StoreId).HasColumnName("store_id");
            modelBuilder.Entity<User>().Property(x => x.Name).HasColumnName("name").HasMaxLength(50).IsRequired();
            modelBuilder.Entity<User>().Property(x => x.Email).HasColumnName("email").HasMaxLength(50).IsRequired();
            modelBuilder.Entity<User>().Property(x => x.Password).HasColumnName("password").HasMaxLength(100)
                .IsRequired();

            modelBuilder.Entity<Role>().ToTable("roles");
            modelBuilder.Entity<Role>().Property(x => x.Id).HasColumnName("id");
            modelBuilder.Entity<Role>().Property(x => x.RoleName).HasColumnName("name").HasMaxLength(50).IsRequired();
        }
    }
}