﻿using System;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.User.Queries
{
    public class GetAllUsers : AllQueryBase<DataModels.User>
    {
        public Guid? StoreId { get; set; }
    }
}