﻿using System;
using MicroBoost.Types;

namespace FastO.Microservices.User.DataModels
{
    public class Role : EntityBase<Guid>
    {
        /// <summary>
        /// Role Id
        /// </summary>
        public override Guid Id { get; set; } = Guid.NewGuid();
        
        /// <summary>
        /// Name of Role
        /// </summary>
        public string RoleName { get; set; }
    }
}