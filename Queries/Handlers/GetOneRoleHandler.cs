﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.User.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.User.Queries.Handlers
{
    public class GetOneRoleHandler : IQueryHandler<GetOneRole, Role>
    {
        private readonly IRepository<Role, Guid> _roleRepository;

        public GetOneRoleHandler(IRepository<Role, Guid> roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public async Task<Role> Handle(GetOneRole request, CancellationToken cancellationToken)
        {
            var role = await _roleRepository.FindOneAsync(request.Id, cancellationToken)
                       ?? throw new NullReferenceException($"Role id {request.Id} does not exist");
            return role;
        }
    }
}