﻿using System;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.User.Commands
{
    public class CreateRole : CommandBase
    {
        [MessageKey] public Guid Id { get; set; } = Guid.NewGuid();
        
        public string RoleName { get; set; }
    }
}