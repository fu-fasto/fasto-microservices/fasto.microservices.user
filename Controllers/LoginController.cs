﻿using System.Threading;
using FastO.Microservices.User.Services;
using MicroBoost.Auth.Identity.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.User.Controllers
{
    [ApiController]
    [Route("auth")]
    public class LoginController : ControllerBase
    {
        private readonly ILoginService _loginService;

        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginRequest loginRequest, CancellationToken cancellationToken)
        {
            LoginResponse response = _loginService.Authenticate(loginRequest);

            if (response == null)
                return BadRequest(new {message = "Username or password is incorrect"});
            
            return Ok(response);
        }
    }
}