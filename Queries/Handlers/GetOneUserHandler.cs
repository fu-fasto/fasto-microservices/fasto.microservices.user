﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.User.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.User.Queries.Handlers
{
    public class GetOneUserHandler : IQueryHandler<GetOneUser, DataModels.User>
    {
        private readonly IRepository<DataModels.User, Guid> _userRepository;
        private readonly IRepository<Role, Guid> _roleRepository;

        public GetOneUserHandler(IRepository<DataModels.User, Guid> userRepository, IRepository<Role, Guid> roleRepository)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }
        
        public async Task<DataModels.User> Handle(GetOneUser request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.FindOneAsync(request.Id, cancellationToken)
                ?? throw new NullReferenceException($"User with id={request.Id} does not exist!");
            user.Role = await _roleRepository.FindOneAsync(user.RoleId, cancellationToken);
            return user;
        }
    }
}