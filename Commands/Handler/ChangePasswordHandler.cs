﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Persistence;
using BC = BCrypt.Net.BCrypt;

namespace FastO.Microservices.User.Commands.Handler
{
    public class ChangePasswordHandler : ICommandHandler<ChangePassword>
    {
        private readonly IRepository<DataModels.User, Guid> _userRepository;

        public ChangePasswordHandler(IRepository<DataModels.User, Guid> userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task Handle(ChangePassword command, CancellationToken cancellationToken)
        {
            var user = await _userRepository.FindOneAsync(command.Id, cancellationToken)
                       ?? throw new NullReferenceException($"User with id={command.Id} does not exist");

            if (BC.Verify(command.OldPwd, user.Password))
            {
                if (command.NewPwd.Equals(command.RetypeNewPwd))
                {
                    user.Password = BC.HashPassword(command.NewPwd);
                }
                else
                {
                    throw new Exception("You must enter the same new password");
                }
            }
            else
            {
                throw new Exception("Old password is wrong");
            }

            await _userRepository.UpdateAsync(user, cancellationToken);
        }
    }
}