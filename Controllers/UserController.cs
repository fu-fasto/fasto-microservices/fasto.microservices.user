﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.User.Commands;
using FastO.Microservices.User.Queries;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.User.Controllers
{
    [ApiController]
    [Route("users")]
    public class UserController : ControllerBase
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryBus _queryBus;

        public UserController(ICommandBus commandBus, IQueryBus queryBus)
        {
            _commandBus = commandBus;
            _queryBus = queryBus;
        }
        
        [HttpGet]
        public async Task<IActionResult> GetAllUsers([FromQuery] GetAllUsers query, CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(query, cancellationToken);
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneUser([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new GetOneUser {Id = id}, cancellationToken);
            return Ok(result);
        }

        [HttpGet("browse")]
        public async Task<IActionResult> BrowseUsers([FromQuery] BrowseUsers query, CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(query, cancellationToken);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] CreateUser command, CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }

        [HttpPatch]
        public async Task<IActionResult> UpdateUser([FromBody] UpdateUser command, CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }

        [HttpPatch("change-pwd")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePassword command,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }
    }
}